package ru.deathless_story.db.dao.factory;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author : ohotNik
 * Date : 18.01.2012
 * Time : 8:32:24
 */
public class SpringConfigurer {

    private static ClassPathXmlApplicationContext context;

    /**
     * Получит контекст спринга.
     * @return  контекст
     */
    public ClassPathXmlApplicationContext getContext() {
        if (context == null) {
            context = new ClassPathXmlApplicationContext("spring-config.xml");
        }
        return context;
    }
}
