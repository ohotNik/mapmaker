package ru.deathless_story.db.dao;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 07.08.13
 * Time: 19:08
 * @param <T>   любой сохраняемый объект
 */
public class GenericDao<T> extends HibernateDaoSupport {

    /**
     * Сохраняет или обновляет объект в бд.
     * @param object    объект
     */
    @Transactional
    public void create(T object) {
        getHibernateTemplate().saveOrUpdate(object);
    }

}
