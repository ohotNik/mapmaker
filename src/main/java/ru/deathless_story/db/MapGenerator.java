package ru.deathless_story.db;

import ru.deathless_story.MapBuffer;
import ru.deathless_story.db.dao.GenericDao;
import ru.deathless_story.db.dao.factory.SpringConfigurer;
import ru.deathless_story.db.entity.MapPoint;

import java.util.Random;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 02.10.13
 * Time: 11:03
 */
public class MapGenerator {

    private int altitude = 180;
    private int longitude = 180;
    private int seeds = 1;
    private int deep = 100;

    public void generate() {
        MapBuffer mapBuffer = new MapBuffer();

        // загрузка воды
        for (int x = 1; x <= longitude; x++) {
            for (int y = 1; y <= altitude; y++) {
                MapPoint mapPoint = new MapPoint();
                mapPoint.setX(x);
                mapPoint.setY(y);
                mapPoint.setType(0);
                mapBuffer.addPoint(mapPoint);
            }
        }

        Random r = new Random();

        for (int i = 0; i < seeds; i++) {
            int x = r.nextInt(longitude - 1) + 1;
            int y = r.nextInt(altitude - 1) + 1;

            for (int d = 0; d < deep; d++) {
                mapBuffer.setType(y, x, 1);
                int way = r.nextInt(4);
                // движение вверх
                if (way == 0) {
                    if (y == altitude) {
                        y = 1;
                    } else {
                        y++;
                    }
                    mapBuffer.setType(y,x,1);
                }
                // движение вправо
                if (way == 1) {
                    if (x == longitude) {
                        x = 1;
                    } else {
                        x++;
                    }
                    mapBuffer.setType(y,x,1);
                }
                // движение вниз
                if (way == 2) {
                    if (y == 1) {
                        y = altitude;
                    } else {
                        y--;
                    }
                    mapBuffer.setType(y,x,1);
                }
                // движение влево
                if (way == 3) {
                    if (x == 1) {
                        x = longitude;
                    } else {
                        x--;
                    }
                    mapBuffer.setType(y,x,1);
                }
            }
        }
    }

    public void publish() {
        MapBuffer mapBuffer = new MapBuffer();
        Set<MapPoint> set = mapBuffer.getPoints();
        GenericDao genericDao = (GenericDao) new SpringConfigurer().getContext().getBean("genericDao");
        for (MapPoint p : set) {
            genericDao.create(p);
        }
    }

}
