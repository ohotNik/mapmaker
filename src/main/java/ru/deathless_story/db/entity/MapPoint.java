package ru.deathless_story.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: OKHONCHENKOAV
 * Date: 31.07.13
 * Time: 17:15
 */

@Entity
@Table(name = "map")
public class MapPoint {

    @Id
    @GeneratedValue
    private String id;

    @Column
    private int x;

    @Column
    private int y;

    @Column
    private int city;

    @Column
    private int type;

    /**
     * получить id.
     *
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * задать id.
     *
     * @param id id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * получить x-координату.
     *
     * @return x
     */
    public int getX() {
        return x;
    }

    /**
     * установить x-координату.
     *
     * @param x x
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * получить y-координату.
     *
     * @return y
     */
    public int getY() {
        return y;
    }

    /**
     * установить Y координату.
     *
     * @param y y
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * получить id города в этой точке.
     *
     * @return id города
     */
    public int getCity() {
        return city;
    }

    /**
     * задать id города в этой точке.
     *
     * @param city id города
     */
    public void setCity(int city) {
        this.city = city;
    }

    /**
     * получить тип местности.
     *
     * @return тип местности
     */
    public int getType() {
        return type;
    }

    /**
     * задать тип местности.
     *
     * @param type тип местности
     */
    public void setType(int type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MapPoint mapPoint = (MapPoint) o;

        if (city != mapPoint.city) return false;
        if (type != mapPoint.type) return false;
        if (x != mapPoint.x) return false;
        if (y != mapPoint.y) return false;
        if (id != null ? !id.equals(mapPoint.id) : mapPoint.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + x;
        result = 31 * result + y;
        result = 31 * result + city;
        result = 31 * result + type;
        return result;
    }
}
