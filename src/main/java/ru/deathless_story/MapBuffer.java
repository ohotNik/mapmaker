package ru.deathless_story;

import ru.deathless_story.db.entity.MapPoint;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 30.09.13
 * Time: 0:00
 */
public class MapBuffer {

    private static Set<MapPoint> points = new HashSet<MapPoint>();

    public void addPoint(MapPoint mapPoint) {
        points.add(mapPoint);
    }

    public void setType(int a, int l, int type) {
        for (MapPoint p : points) {
            if (p.getX() == l && p.getY() == a) {
                points.remove(p);
                p.setType(type);
                points.add(p);
                return;
            }
        }
    }

    public Set<MapPoint> getPoints() {
        return points;
    }

}
