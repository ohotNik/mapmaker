package ru.deathless_story;

import ru.deathless_story.db.MapGenerator;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 29.09.13
 * Time: 23:53
 */
public class MC {

    public static void main(String[] args) {
        long sTime = System.currentTimeMillis();
        MapGenerator mapGenerator = new MapGenerator();
        mapGenerator.generate();
        System.out.print("Завершена генерация карты за " + (System.currentTimeMillis() - sTime) + "ms.");
        mapGenerator.publish();
        System.out.print("Завершены генерация и загрузка карты за " + (System.currentTimeMillis() - sTime) + "ms.");
    }

}
